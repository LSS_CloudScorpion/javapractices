
import Processors.Equation;
import Exceptions.InvalidEquationException;
import Exceptions.InvalidOperatorException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CloudScorpion
 */

public class EquationTest
   {
   
   public static void main(String[] args)
      {
      Equation eq = new Equation();
      
      String[] eqs = new String[4];
      eqs[0] = "17 + 3";
      eqs[1] = "17 + 3 * 2";
      eqs[2] = "(17 + 3) * 2";
      eqs[3] = "(((17 + 3) / (3 - 1)) * 4)";
      
      
      double[] results = new double[4];
              
      try
         {
         for(int i = 0; i < 4; i++)
            {
            System.out.println("Running Calculation: " + eqs[i]);
            eq.parseEquation(eqs[i]);
            results[i] = eq.solve();
            }
         }
      catch(InvalidOperatorException ex)
         {
         System.out.println(ex.getMessage());
         }
      catch(InvalidEquationException ex)
         {
         System.out.println(ex.getMessage());
         }
      
      for(int i =0; i < 4; i++)
         {
         System.out.println("Result was: " + Double.toString(results[i]));
         }
      }
   
   }
