/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Application;
import Processors.Calculator;
import Exceptions.*;
import GUI.GUI;
import java.util.Scanner;
/**
 *
 * @author CloudScorpion
 */
public class Application
   {
   
   public static void ConsoleMode(Calculator c)
      {
      boolean cont = true;
      System.out.println("Entering console mode...");
      Scanner in = new Scanner(System.in);
      while(cont)
         {
         String input = in.nextLine();
         if(input.equalsIgnoreCase("help"))
            System.out.println("Enter an equation or a console command\n\n"
                    + "Console commands:\n"
                    + "help - This help menu\n"
                    + "mem - Store last equation in memory\n"
                    + "history - List all previous equations\n"
                    + "quit - Quits this program");
         else if(input.equalsIgnoreCase("history"))
            for(int i = 0; i < c.getHistorySize(); i++)
               System.out.println(c.getHistoryEntry(i));
         else if(input.equalsIgnoreCase("mem"))
            {
            if(c.getHistorySize() == 0)
               System.out.println("No equation has been entered");
            else
               c.storeMemory(c.getHistoryEntry(c.getHistorySize() - 1));
            }
         else if(input.equalsIgnoreCase("quit"))
            cont = false;
         else//equation assumed
            {
            try
               {
               double solved = c.solve(input);
               System.out.println(solved);
               }
            catch(InvalidOperatorException e)
               {
               System.out.println(e.getMessage());
               }
            catch (InvalidEquationException e)
               {
               System.out.println("Invalid Equation: " + e.getMessage());
               }
            }
         }
      }
   
   public static void main(String[] args)
      {
      Calculator c = new Calculator();
      
      if(args.length > 0)
         {
         if(args[0].charAt(0) == '-') //command
            {
            if(args[0].length() != 2)
               System.out.println("Invalid option provided");
            else
               {
               switch(args[0].charAt(1))
                  {
                  case 'h':
                  case 'H':
                     System.out.println("Basic calculator program\nOptions:\n-h - This help menu\n-c - Console mode\n[equation] - Processes equation and outputs reslut");
                     break;
                  case 'c':
                  case 'C':
                     ConsoleMode(c);
                  }
               }
            }
         else //inline equation
            {
            try
               {
               double s = c.solve(args[0]);
               System.out.println(Double.toString(s));
               }
            catch(InvalidOperatorException|InvalidEquationException ex)
               {
               System.out.println(ex.getMessage());
               }
            }
         }
      else //gui mode
         {
         java.awt.EventQueue.invokeLater(new Runnable()
            {
            public void run()
               {
               new GUI(c).setVisible(true);
               }
            });
         
         
         }
      
      }
   }
