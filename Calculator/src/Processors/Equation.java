package Processors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CloudScorpion
 */
import Exceptions.InvalidEquationException;
import Exceptions.InvalidOperatorException;
import java.util.Vector;


public class Equation
   {
   private Vector<String> args;
   
   public Equation()
      {
      args = new Vector<String>();
      }
   
   private void validateBank(String bank) throws InvalidOperatorException
      {
      //Ensure there are not 2 or more dots
      boolean dotFound = false;
      for(int j = 0; j < bank.length(); j++)
         {
         if(bank.charAt(j) == '.')
            {
            if(dotFound)
               throw new InvalidOperatorException("Multiple . found in number");
            else
               dotFound = true;
            }
         }
      
      }
   
   public void parseEquation(String eq) throws InvalidOperatorException
      {
      args.clear();
      
      String bank = new String();
      
      for(int i = 0; i < eq.length(); i++)
         {
         char a = eq.charAt(i);
         
         if(Character.isDigit(a) || a == '.')
            bank = bank + a;
         else
            {
            if(bank.length() > 0)
               {
               validateBank(bank);
               args.add(bank);
               bank = new String();
               }
            
            String sym = Character.toString(a);
            
            if(sym.matches("[\\^\\*\\-\\+\\/\\%\\)\\(]"))
               args.add(sym);
            else if(Character.getType(a) != Character.SPACE_SEPARATOR)
               throw new InvalidOperatorException("Invalid Operator \"" + 
                       Character.toString(a) + "\"");
            }
         }
      if(bank.length() > 0)
         {
         validateBank(bank);
         args.add(bank);
         }
      }
   
   public double solve() throws InvalidEquationException
      {
      double result;
      
      //find and solve parathesis first
      for(int i = 0; i < args.size(); i++)
         {
         if(args.get(i).equals("("))
            {
            if(i == args.size() -1)
               throw new InvalidEquationException("Missing closing parenthesis");
            int end = 0;
            Equation inner = new Equation();
            int skipCount = 0;
            for(int j = i+1; j < args.size() && end == 0; j++)
               {
               if(args.get(j).equals("("))
                  {
                  skipCount++;
                  inner.args.add(args.get(j));
                  }
               else if(args.get(j).equals(")"))
                  {
                  if(skipCount > 0)
                     {
                     skipCount--;
                     inner.args.add(args.get(j));
                     }
                  else
                     {
                     if(inner.args.isEmpty())
                        throw new InvalidEquationException("Parenthesis set empty");
                     end = j;
                     }
                  }
               else
                  inner.args.add(args.get(j));
               }
            if(end == 0)
               throw new InvalidEquationException("Missing closing parenthesis");
            while(end != i-1)
               args.remove(end--);
            args.insertElementAt(Double.toString(inner.solve()), i);
            }
         }
      
      //Append negatives to arguments where appropriate
      for(int i = 0; i < args.size(); i++)
         {
         if(args.get(i).equals("-"))
            {
            if((i == 0 || !args.get(i - 1).matches("-?\\d*\\.?\\d*") || 
                    args.get(i-1).equals("-")))
               {
               args.set(i,args.get(i) + args.get(i+1));
               args.remove(i+1);
               i--;
               }
            }
         }
      
      //Ensure valid equation pattern
      boolean wasNumber = false;
      for(int i = 0; i < args.size(); i++)
         {
         if(!args.get(i).equals("-") && args.get(i).matches("-?\\d*\\.?\\d*"))
            {
            if(wasNumber)
               throw new InvalidEquationException("Equation missing operator");
            wasNumber = true;
            }
         else
            {
            if(!wasNumber)
               throw new InvalidEquationException("Equation missing value");
            wasNumber = false;
            }
         }
      if(!wasNumber)
         throw new InvalidEquationException("Equation missing value");
      
      //Solve for exponents
      for(int i = 0; i < args.size(); i++)
         {
         if(args.get(i).equals("^"))
            {
            args.set(i-1, Double.toString(
                    Math.pow(Double.parseDouble(args.get(i-1)),  
                            Double.parseDouble(args.get(i+1)))));
            args.remove(i);
            args.remove(i--);
            }
         }
      
      //Solve for modulus, multiply, divide
      for(int i = 0; i < args.size(); i++)
         {
         if(args.get(i).equals("*"))
            {
            args.set(i-1, Double.toString(
                    Double.parseDouble(args.get(i-1)) *  
                    Double.parseDouble(args.get(i+1))));
            args.remove(i);
            args.remove(i--);
            }
         else if(args.get(i).equals("/"))
            {
            args.set(i-1, Double.toString(
                    Double.parseDouble(args.get(i-1)) /  
                    Double.parseDouble(args.get(i+1))));
            args.remove(i);
            args.remove(i--);
            }
         else if(args.get(i).equals("%"))
            {
            args.set(i-1, Double.toString(
                    Double.parseDouble(args.get(i-1)) %  
                    Double.parseDouble(args.get(i+1))));
            args.remove(i);
            args.remove(i--);
            }
         }
      
      //Solve for add and subtract
      for(int i = 0; i < args.size(); i++)
         {
         if(args.get(i).equals("+"))
            {
            args.set(i-1, Double.toString(
                    Double.parseDouble(args.get(i-1)) +  
                    Double.parseDouble(args.get(i+1))));
            args.remove(i);
            args.remove(i--);
            }
         else if(args.get(i).equals("-"))
            {
            args.set(i-1, Double.toString(
                    Double.parseDouble(args.get(i-1)) -  
                    Double.parseDouble(args.get(i+1))));
            args.remove(i);
            args.remove(i--);
            }
         }
      
      //this is just a test to see if we messed up
      if(args.size() != 1)
         throw new InvalidEquationException("We probably messed something up");
      
      result = Double.parseDouble(args.get(0));
      
      return result;
      }
   
   public String serialize()
      {
      String result = new String();
      
      for(int i = 0; i < args.size(); i++)
         result += args.get(i);
      
      return result;
      }
   }
