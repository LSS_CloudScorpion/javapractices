package Processors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CloudScorpion
 */
import Exceptions.InvalidEquationException;
import Exceptions.InvalidOperatorException;
import java.util.ArrayList;

public class Calculator
   {
   private ArrayList<Equation> history;
   private String memory;
   
   public Calculator()
      {
      history = new ArrayList<Equation>();
      memory = new String();
      }
   
   public int getHistorySize()
      {
      return history.size();
      }
   
   public final ArrayList<Equation> getHistory()
      {
      return history;
      }
   
   public final String getHistoryEntry(int i)
      {
      return history.get(i).serialize();
      }
   
   public void storeMemory(String mem)
      {
      memory = new String(mem);
      }
   
   public final String recallMemory()
      {
      return memory;
      }
   
   public double solve(String eq) throws InvalidEquationException, InvalidOperatorException
      {
      Equation cur = new Equation();
      
      cur.parseEquation(eq);
      history.add(cur);
      return cur.solve();
      }
   
   }
